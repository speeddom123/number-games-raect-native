import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  Keyboard,
  View,
  TouchableWithoutFeedback,
  Button,
  Alert,
} from "react-native";
import Inputbox from "../Components/Input";
import Card from "../Components/Card";
import NumberCon from "../Components/NumberContainer";
import Colors from "../Constants/Color";
const StartGame = (props) => {
  const [confirm, SetConfirm] = useState(false);
  const [selectedNumber, setNumber] = useState();
  const [enteredValue, SetenteredValue] = useState("");
  const resetInputHandler = () => {
    SetenteredValue("");
    SetConfirm(false);
  };
  const confirmInputHandler = () => {
    const chosenNumber = parseInt(enteredValue);
    if (isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 99) {
      Alert.alert("Invalid number!", "Number must be between 1 and 99", [
        { text: "Okay", style: "destructive", onPress: resetInputHandler },
      ]);
      return;
    }
    SetConfirm(true);
    setNumber(chosenNumber);
    SetenteredValue("");
    Keyboard.dismiss();
  };
  let confirmedOutput;
  if (confirm) {
    confirmedOutput = (
      <Card style={styles.summmaryContainer}>
       
        <Text>You Selected: </Text>
        <NumberCon>{selectedNumber}</NumberCon>
        <Button title="START GAME" color={Colors.primary} onPress={() => props.onStartGame(selectedNumber)} />
      </Card>
    );
  }
  const numberInputHandler = (inputText) => {
    SetenteredValue(inputText.replace(/[^0-9]/g, ""));
  };
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <View style={styles.screen}>
        <Text style={styles.titles}>Start a New Game</Text>
        <Card style={styles.InputContainer}>
          <Text style={styles.title}>Select a Number</Text>
          <Inputbox
            style={styles.input}
            blurOnSubmit
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="number-pad"
            maxLength={2}
            onChangeText={numberInputHandler}
            value={enteredValue}
          />
          <View style={styles.buttonContainer}>
            <View style={styles.button}>
              <Button
                color={Colors.accent}
                onPress={resetInputHandler}
                title="Reset"
              />
            </View>
            <View style={styles.button}>
              <Button
                color={Colors.primary}
                onPress={confirmInputHandler}
                title="Confirm"
              />
            </View>
          </View>
        </Card>
        {confirmedOutput}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    marginVertical: 10,
    fontWeight:"bold"
  },
  InputContainer: {
    width: 300,
    maxWidth: "80%",
    alignItems: "center",
  },
  buttonContainer: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    paddingHorizontal: 15,
  },
  button: {
    width: 100,
  },
  input: {
    width: 50,
    textAlign: "center",
  },
  summmaryContainer:{
      marginTop: 20,
      alignItems: 'center'
  },
  titles:{
    fontSize: 30,
    color: Colors.primary,
    fontWeight:"bold",
   marginVertical: 20
}
});
export default StartGame;
